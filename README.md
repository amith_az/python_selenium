# Jenkins Pytest Selenium Setup Instructions

## Prerequists:
- Docker installed with WSL2 support


## Jenkins Installation:
- Clone https://gitlab.com/amith_az/python_selenium/-/tree/jenkins
- Run `build.sh` to build the jenkins image.
- Run `run.sh` to run the built jenkins image.
- Visit [localhost:8080](http://localhost:8080) to open jenkins.
- If installing for the first time, please do the initial setup. 


## Setting up the Jenkins Project for Pytest Selenium
- Click on 'New Item' and enter a name.
- Click on 'Freestyle project' and press 'OK'.
- Click on 'Source Code Management' and paste the repo link and add auth if needed.
- Go to 'Build', click on 'Add Build Step' and select 'Execute shell'.
- Copy paste whole of `Jenkins Build Script.sh` into the textbox.
- [OPTIONAL] 'Add post-build action' > 'Publish JUnit test result report', and in 'Test report XMLs', paste this `reports/result.xml`
- Click on Apply button and Save button.
- Finally click on 'Build Now'.


## Run Chrome Standalone Docker with VNC Support:
- Run the below code:
```bash
docker run -d -p 4444:4444 -p 5900:5900 --shm-size=2g selenium/standalone-chrome-debug:3.141.59-20210607
```
-   Install VNC viewer application and use the config below to observer the tests being run.
    - VNC Viewer: 127.0.0.1:5900
    - VNC Password: secret