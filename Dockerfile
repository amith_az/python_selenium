FROM python:3.6-slim

ADD appzera_test.py requirements.txt /python-test/

WORKDIR /python-test

RUN pip install --no-cache-dir -r requirements.txt

# to prevent the following lines from caching
ARG CACHEBUST=1 

RUN ["pytest", "appzera_test.py", "--junitxml=reports/result.xml"]

CMD tail -f /dev/null
